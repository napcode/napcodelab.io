install: clean venv
	. venv/bin/activate && pip install -r requirements.txt

venv:
	python -m venv venv

clean: rm-public rm-venv

rm-public:
	rm -rf public

rm-venv:
	rm -rf venv

build:
	. venv/bin/activate && python build.py
	mv zoho-domain-verification.html public/

dev:
	. venv/bin/activate && python build.py --dev
