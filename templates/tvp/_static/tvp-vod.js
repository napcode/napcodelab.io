function get_video_id() {
  const link_input = document.querySelector('#link-input')
  const link = link_input.value 
  const link_parts = link.split(',')
  const id = link_parts[link_parts.length - 1]

  return id
}

function open_video(video_id) {
  const url = 'https://vod.tvp.pl/sess/tvplayer.php?object_id=' + 
    video_id + '&autoplay=true'
  window.open(url, '_blank')
}

window.addEventListener('load', function() {
  const watch_button = document.querySelector('#watch-button')
  watch_button.addEventListener('click', function (event) {
    const video_id = get_video_id()
    open_video(video_id)
  })
})

