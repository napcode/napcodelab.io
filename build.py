import os
import json
import yaml
import sass
import click
import shutil

from pathlib import Path
from staticjinja import Site
from multiprocessing import Process
from http.server import HTTPServer, SimpleHTTPRequestHandler
from watchdog.observers import Observer
from watchdog.events import (
    FileSystemEventHandler,
    EVENT_TYPE_CREATED,
    EVENT_TYPE_MODIFIED,
)

DATA_DIR = "data"
TARGET_DIR = "public"
TEMPLATES_DIR = "templates"
PAGES_DIR = "pages"


def load_data():
    if not os.path.exists(DATA_DIR):
        return {}

    data = {}
    for file_name in os.listdir(DATA_DIR):
        print(f"Load data {file_name}")
        name, file_type = file_name.split(".")
        with open(f"{DATA_DIR}/{file_name}", "r") as f:
            if file_type in ("yaml", "yml"):
                data[name] = yaml.safe_load(f)
            if file_type == "json":
                data[name] = json.load(f)

    return data


def prepare_target_dir():
    if os.path.exists(TARGET_DIR):
        print(f"Remove {TARGET_DIR}")
        shutil.rmtree(TARGET_DIR)
    print(f"Create {TARGET_DIR}")
    os.mkdir(TARGET_DIR)


def copy_static_assets():
    p = Path(TEMPLATES_DIR)
    for path in p.glob("**/_static"):
        parts = path.parts
        dst = "/".join([TARGET_DIR, *parts[1:-1], "static"])
        print(f"Copytree {path}")
        shutil.copytree(src=path, dst=dst, dirs_exist_ok=True)


def copy_static_pages():
    p = Path(PAGES_DIR)
    for path in p.glob("*"):
        parts = path.parts
        dst = "/".join([TARGET_DIR, parts[-1]])
        print(f"Copytree {path}")
        shutil.copytree(src=path, dst=dst, dirs_exist_ok=True)


def build_css():
    print("Build css")
    css = sass.compile(filename="scss/base.scss", output_style="compressed")
    with open(f"{TEMPLATES_DIR}/_static/base.min.css", "w") as f:
        f.write(css)


def build_filters():
    return {}


def build(site):
    prepare_target_dir()
    build_css()
    copy_static_assets()
    copy_static_pages()
    site.render()


class EventHandler(FileSystemEventHandler):
    def __init__(self, site):
        self.site = site

    def should_handle(self, event):
        return not event.is_directory and event.event_type in (
            EVENT_TYPE_MODIFIED,
            EVENT_TYPE_CREATED,
        )

    def on_any_event(self, event):
        if self.should_handle(event):
            if "_static" in event.src_path:
                copy_static()
            else:
                self.site.render()


class HttpRequestHandler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=TARGET_DIR, **kwargs)


def start_server():
    server_address = ("", 8000)
    httpd = HTTPServer(server_address, HttpRequestHandler)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
        print("\nHTTP server closed")


def watch(site):
    observer = Observer()
    observer.schedule(EventHandler(site), TEMPLATES_DIR, recursive=True)
    observer.start()
    server_process = Process(target=start_server)
    server_process.deamon = True
    server_process.start()
    try:
        while observer.is_alive() and server_process.is_alive():
            observer.join(1)
    finally:
        observer.stop()
        observer.join()
        server_process.join()


@click.command()
@click.option("--dev", is_flag=True)
def main(dev):
    site = Site.make_site(
        searchpath=TEMPLATES_DIR,
        outpath=TARGET_DIR,
        filters=build_filters(),
        env_globals={"G": {"dev": dev}, "data": load_data()},
    )
    build(site)

    if dev:
        watch(site)


if __name__ == "__main__":
    main()
