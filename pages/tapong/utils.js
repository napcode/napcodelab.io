function getRandomUser() {
    const main = Alpine.store("main")

    if (main.bounced) {
        const players = main.players.filter(x => x != main.userName)
        return players[Math.floor(Math.random() * players.length)]
    }

    const nextUser = main.players[Math.floor(Math.random() * main.players.length)];

    if (nextUser == main.userName) {
        main.bounced = true
    }

    return nextUser
}