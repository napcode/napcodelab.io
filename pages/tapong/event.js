const Event = {
    PlayerJoined: 'player_joined',
    PlayerAccepted: 'player_accepted',
    GameStarted: 'game_started',
    PlayerSucceded: 'player_succeded',
    PlayerFailed: 'player_failed',
    PlayerWon: 'player_won',
    DecreasedTime: 'decreased_time'
}

function sendPlayerJoined() {
    const main = Alpine.store("main")

    const msg = {
        event: Event.PlayerJoined,
        userName: main.userName
    }

    window.socket.send(JSON.stringify(msg))
}

function sendPlayers() {
    const main = Alpine.store("main")

    const msg = {
        event: Event.PlayerAccepted,
        players: main.joinedPlayers
    }

    window.socket.send(JSON.stringify(msg))
}


function sendGameStarted() {
    const main = Alpine.store("main")
    main.players = main.joinedPlayers

    const msg = {
        event: Event.GameStarted,
        nextPlayer: getRandomUser()
    }

    window.socket.send(JSON.stringify(msg))
}

function sendPlayerSucceded() {
    const msg = {
        event: Event.PlayerSucceded,
        nextPlayer: getRandomUser()
    }

    window.socket.send(JSON.stringify(msg))
}

function sendPlayerFailed() {
    const main = Alpine.store("main")
    main.players = main.players.filter(x => x != main.userName)

    const msg = {
        event: Event.PlayerFailed,
        players: main.players,
        nextPlayer: getRandomUser(),
        ranking: main.ranking
    }

    window.socket.send(JSON.stringify(msg))
}

function sendPlayerWon() {
    const main = Alpine.store("main")
    main.players = []

    const msg = {
        event: Event.PlayerWon,
        players: main.players,
        ranking: main.ranking
    }

    window.socket.send(JSON.stringify(msg))
}

function sendDecreasedTime() {
    const main = Alpine.store("main")

    const msg = {
        event: Event.DecreasedTime,
        time: main.lightTime
    }

    window.socket.send(JSON.stringify(msg))
}

