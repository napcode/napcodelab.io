// const WS_URL = '134.209.227.132:8000'
const SERVER_URL = 'sky.napcode.eu'

async function createGame(userName) {
    const main = Alpine.store("main")

    main.joinedPlayers = []
    main.userName = userName
    main.isMaster = true
    const response = await fetch(`https://${SERVER_URL}/hub/create`)
    const room = await response.text()
    main.room = room
    main.currentPage = Page.Lobby

    connect()
}


function connect() {
    console.log("Connect")
    const main = Alpine.store("main")
    window.socket = new WebSocket(`wss://${SERVER_URL}/hub/join/${main.room}`)
    socket.onerror = (event) => {
        console.log(event)
    }
    socket.addEventListener("message", (event) => {
        const main = Alpine.store("main")

        const ed = JSON.parse(event.data)
        if (ed.sender == "server") {
            return;
        }

        const e = JSON.parse(ed.data)

        if (e.event == Event.PlayerJoined && main.isMaster) {
            main.joinedPlayers.push(e.userName)
            sendPlayers()
            return
        }

        if (e.event == Event.PlayerAccepted) {
            main.joinedPlayers = e.players
        }

        if (e.event == Event.GameStarted) {
            main.restartGame()

            if (e.nextPlayer == main.userName) {
                main.receiveImpulse()
            } else {
                main.bounced = false
            }
        }

        if (e.event == Event.PlayerSucceded) {
            main.counter += 1

            if (e.nextPlayer == main.userName) {
                main.receiveImpulse()
            } else {
                main.bounced = false
            }

            if (main.isMaster && main.counter % DECREASE_TIME_THRESHOLD == 0) {
                const timeModifier = Math.floor(main.counter / DECREASE_TIME_THRESHOLD)
                main.lightTime = 500 - timeModifier * 50

                sendDecreasedTime()
            }
        }

        if (e.event == Event.PlayerFailed) {
            main.players = e.players
            main.ranking = e.ranking

            if (e.players.length == 1 && e.nextPlayer == main.userName) {
                main.ranking[main.players.length] = main.userName
                sendPlayerWon()

                return
            }

            if (e.nextPlayer == main.userName) {
                main.receiveImpulse()
            } else {
                main.bounced = false
            }
        }

        if (e.event == Event.PlayerWon) {
            main.players = e.players
            main.ranking = e.ranking
            main.currentPage = Page.GameOver
            main.gameFinished = true
        }

        if (e.event == Event.DecreasedTime) {
            main.lightTime = e.time
        }
    })

    socket.addEventListener("open", (event) => {
        console.log("Connected")
        sendPlayerJoined()
    })
}

const DECREASE_TIME_THRESHOLD = 3
