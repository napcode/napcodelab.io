'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"flutter_bootstrap.js": "3822451dbe25463340ee2c01a6f09c36",
"version.json": "3e2b46ac8105fa9b5b3fd9b1ffc34fda",
"index.html": "85baf816e2cefec930de2478a57a165a",
"/": "85baf816e2cefec930de2478a57a165a",
"main.dart.js": "cf24fa644a6eb944471fce11d243a07e",
"flutter.js": "383e55f7f3cce5be08fcf1f3881f585c",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/singeland.png": "5abc45793c742fa521e6513b2fcf1791",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "310960d30cebc647fc841adcb954e7b3",
"assets/AssetManifest.json": "7f1892f2a08d26314260a42f1d54d1eb",
"assets/NOTICES": "c9b473409cef63c2e4ed5f878781d227",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/AssetManifest.bin.json": "90895d8d82ed07c4f4a760735d32f306",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "e986ebe42ef785b27164c36a9abc7818",
"assets/shaders/ink_sparkle.frag": "ecc85a2e95f5e9f53123dcaf8cb9b6ce",
"assets/AssetManifest.bin": "821e9c8ebc2a0b15739ee46ea0ded56a",
"assets/fonts/MaterialIcons-Regular.otf": "0db35ae7a415370b89e807027510caf0",
"assets/assets/background/wave_side.svg": "da3e21dbbcd48997e2d370ee3531a476",
"assets/assets/background/notes_small.svg": "7657e216c9c2989bb4c77921e816cc51",
"assets/assets/background/notes_big.svg": "8202d2a38ebe3cf391af7cfa7ecb38e2",
"assets/assets/background/wave.svg": "f399f0fa6cad09e85075046cc2d8d3b5",
"assets/assets/images/dog.png": "2c0bafe1af81ca6edaff3b11880d35f1",
"assets/assets/images/charlie_gru_shadow.png": "5e755bdae2a0b52837d738816ecd44c6",
"assets/assets/images/bakabu-logo.png": "5f42c537de89e1b1f44f4baca85ae6b9",
"assets/assets/images/pigeon_shadow.png": "6b31f7528ab5ab57676f7fe5168aaea8",
"assets/assets/images/crow_shadow.png": "2face0ed592cb8470ea1e97f917ad537",
"assets/assets/images/charlie_gru.png": "b1f0008b31d2892c45ad55bda8985856",
"assets/assets/images/cat_shadow.png": "40c29748afd523d1aaba5df8ef7bfee7",
"assets/assets/images/cow_shadow.png": "ace9b2df921ac9a8c02e9cb85e1bb30e",
"assets/assets/images/bakabu.png": "4b5c5b9cf5230862535f7bf26010301d",
"assets/assets/images/mimi_lou_shadow.png": "93b60477937affd63b091e1c04f50f20",
"assets/assets/images/kornett.png": "c29c4f29e6f4022ef6bcf89aeb18dccc",
"assets/assets/images/anton_shadow.png": "683fcd08ba17c24a816e199be482a897",
"assets/assets/images/cow.png": "1452ba44ad85045c6f3d3e680358d975",
"assets/assets/images/embalo.png": "f99d9813f3e65c0a2097a6a758399f77",
"assets/assets/images/donkey.png": "61436b7414ab82483023372992a7a99a",
"assets/assets/images/crow.png": "c2e5c53979bf7c0f428bd0f52dae258b",
"assets/assets/images/donkey_shadow.png": "40d23f4470e0f35a8bcd35b56d0d3be8",
"assets/assets/images/embalo_shadow.png": "4e01ebf7c131c66e7dfc8b22ddbc2b52",
"assets/assets/images/kornett_shadow.png": "9caa72abd08816ebed340525e0176296",
"assets/assets/images/cat.png": "caccfb713529dab932d684a74a3a42a1",
"assets/assets/images/ukuleila_shadow.png": "faf93a51998bc424d45a4a5504260c66",
"assets/assets/images/rocket.png": "2f23722d19b4eb27aeeeb1841e3240d9",
"assets/assets/images/pigeon.png": "f8d272176841cbed72c4eda2d54ed073",
"assets/assets/images/dog_shadow.png": "580648f75d666ba2f6846521c61483b4",
"assets/assets/images/ukuleila.png": "2d331520d948e6700886cb63c74af07f",
"assets/assets/images/anton.png": "c8ccfb657dc44e595e4f43b4ff3e7e09",
"assets/assets/images/bakabu_shadow.png": "a9918082ff0e49adb47071bb666caeda",
"assets/assets/images/mimi_lou.png": "b4c1a9aec4274f0b0b66fc958c92a27c",
"assets/assets/audio/smoke.mp3": "168c8d3bd9212f8fbbe883dcae7d5e58",
"assets/assets/audio/wrong.mp3": "37b297ff47c6b8eed9852b61dad09678",
"assets/assets/audio/correct_short.mp3": "2f36b9a968b360bff0c516672f5a9981",
"assets/assets/audio/correct.mp3": "a46924ebe3a3b231be1bb686fe99701d",
"assets/assets/animations/transition.json": "82c70e2ef0a24bbbf2cf992043c82267",
"canvaskit/skwasm.js": "5d4f9263ec93efeb022bb14a3881d240",
"canvaskit/skwasm.js.symbols": "c3c05bd50bdf59da8626bbe446ce65a3",
"canvaskit/canvaskit.js.symbols": "74a84c23f5ada42fe063514c587968c6",
"canvaskit/skwasm.wasm": "4051bfc27ba29bf420d17aa0c3a98bce",
"canvaskit/chromium/canvaskit.js.symbols": "ee7e331f7f5bbf5ec937737542112372",
"canvaskit/chromium/canvaskit.js": "901bb9e28fac643b7da75ecfd3339f3f",
"canvaskit/chromium/canvaskit.wasm": "399e2344480862e2dfa26f12fa5891d7",
"canvaskit/canvaskit.js": "738255d00768497e86aa4ca510cce1e1",
"canvaskit/canvaskit.wasm": "9251bb81ae8464c4df3b072f84aa969b",
"canvaskit/skwasm.worker.js": "bfb704a6c714a75da9ef320991e88b03"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"flutter_bootstrap.js",
"assets/AssetManifest.bin.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
