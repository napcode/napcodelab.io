'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"flutter_bootstrap.js": "4aa4e01a269b126058842b8f55830d97",
"version.json": "9d2822613cce49cbb418b4e4c5edab39",
"index.html": "1fe244c368068ba44be0ffe7d7e5f172",
"/": "1fe244c368068ba44be0ffe7d7e5f172",
"main.dart.js": "a3cda4d233f9133e146a547cad7df538",
"flutter.js": "383e55f7f3cce5be08fcf1f3881f585c",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/singeland.png": "5abc45793c742fa521e6513b2fcf1791",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "d3a221a3bee0a2faeab05d638bd8ea6e",
"assets/AssetManifest.json": "d3cb9e59b5011051f023cc2bca0179a6",
"assets/NOTICES": "970750144931a0af3d614732f7ffb1ff",
"assets/FontManifest.json": "fab96f751688669ab1be76dab67cd659",
"assets/AssetManifest.bin.json": "213735ffb79327ef9381f53aa97902c8",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "89ed8f4e49bcdfc0b5bfc9b24591e347",
"assets/shaders/ink_sparkle.frag": "ecc85a2e95f5e9f53123dcaf8cb9b6ce",
"assets/AssetManifest.bin": "6e5b1f779e2ccf7cf082781a1564879b",
"assets/fonts/MaterialIcons-Regular.otf": "0db35ae7a415370b89e807027510caf0",
"assets/assets/bakabu/bakabu-logo.png": "5f42c537de89e1b1f44f4baca85ae6b9",
"assets/assets/bakabu/meine_erste_animals/background.svg": "64aefbef67bd47fc014c3b5d1a22ae39",
"assets/assets/bakabu/meine_erste_animals/sound_yellow_inactive.svg": "a8ef59e615bd69478709f06cdee769bb",
"assets/assets/bakabu/meine_erste_animals/sound_card_background.svg": "5d3649092f56c906cd1c308404417b85",
"assets/assets/bakabu/meine_erste_animals/images/dog.png": "ee5784a86f4856909ea717736fd8cd48",
"assets/assets/bakabu/meine_erste_animals/images/cow.png": "6f7ca82706b1106fdaa0c68924dfbf90",
"assets/assets/bakabu/meine_erste_animals/images/donkey.png": "52916fe2e97a109644162fba895ac9a7",
"assets/assets/bakabu/meine_erste_animals/images/crow.png": "282ce41869c7770202a249f485e5ace1",
"assets/assets/bakabu/meine_erste_animals/images/cat.png": "c355b125e4b623733da54d39d7fb4841",
"assets/assets/bakabu/meine_erste_animals/images/pigeon.png": "a02af34f60af72e574de36680f1accb2",
"assets/assets/bakabu/meine_erste_animals/sound_pink_selected.svg": "7772247cdb0ec796b25c593af8498a00",
"assets/assets/bakabu/meine_erste_animals/sound_yellow.svg": "e50604c68b6050e4e73b2695eb7725cf",
"assets/assets/bakabu/meine_erste_animals/audio/cow.wav": "49691f110e0e3223f4a71b9f4a47fd4d",
"assets/assets/bakabu/meine_erste_animals/audio/crow.wav": "cf2bc07908b89f4c16c71f4649102a87",
"assets/assets/bakabu/meine_erste_animals/audio/donkey.wav": "f44a155dba61b60f3ac8179ab6e23b2b",
"assets/assets/bakabu/meine_erste_animals/audio/dog.wav": "20adbdd1233631702295797e7d24a554",
"assets/assets/bakabu/meine_erste_animals/audio/cat.wav": "fb2e416b643295754aee0c7659a82dcd",
"assets/assets/bakabu/meine_erste_animals/audio/celebration.wav": "2514d8506c7dd0cd833b43536625d86a",
"assets/assets/bakabu/meine_erste_animals/audio/pigeon.wav": "8ff073b23b74765326064e41b766d1e6",
"assets/assets/bakabu/meine_erste_animals/sound_pink_unactive.svg": "8e5eeb4a8f67d4854ddd42cffeed8257",
"assets/assets/bakabu/meine_erste_animals/animations/pink_sound.json": "fc82c41724228a9e2c49a046ca33384b",
"assets/assets/bakabu/wave_side.svg": "da3e21dbbcd48997e2d370ee3531a476",
"assets/assets/bakabu/notes_small.svg": "7657e216c9c2989bb4c77921e816cc51",
"assets/assets/bakabu/instruments/images/piano.png": "01769863a2f97778b3911538ecb9fbb6",
"assets/assets/bakabu/instruments/images/drums.png": "cc787c14f8e07cac2d2f974d03777ed6",
"assets/assets/bakabu/instruments/images/flute.png": "8ac652545e84dab9665514bb7e1ac706",
"assets/assets/bakabu/instruments/images/accordion.png": "ccf8ab9526b8265d24fa4b23d868c9df",
"assets/assets/bakabu/instruments/images/triangle.png": "16c436477b1fa902df254f91b4846e1d",
"assets/assets/bakabu/instruments/images/violin.png": "34f2a7e931a12be586c32ce66b4ddd1c",
"assets/assets/bakabu/instruments/images/chimes.png": "589cd49d39f55b28cc015ac6fb588645",
"assets/assets/bakabu/instruments/images/guitar.png": "d3b3420eb5604303f6f80b5e8025fc09",
"assets/assets/bakabu/instruments/images/trumpet.png": "fd0708cd9d4e0d694b4c77fc5722908d",
"assets/assets/bakabu/instruments/images/contrabass.png": "b6327ba7d051dc4dd14ca3f1b73f62f0",
"assets/assets/bakabu/instruments/images/clarinete.png": "d1ff9d1d8cdcdc39cd64d3484af90d83",
"assets/assets/bakabu/instruments/audio/accordion.wav": "1e6b7458f25ddcf121d6a94ae0969523",
"assets/assets/bakabu/instruments/audio/triangle.wav": "b6c7457d8f642de14eaca809bb298bb1",
"assets/assets/bakabu/instruments/audio/violin.wav": "b811a3aa499e87038b4f8513394aad4e",
"assets/assets/bakabu/instruments/audio/drums.wav": "1386d63ec3c6b7757fdc15768f5b954e",
"assets/assets/bakabu/instruments/audio/piano.wav": "04def63010600652f41de0ab0808c4d9",
"assets/assets/bakabu/instruments/audio/celebration.mp3": "525fafe7a8741f70809c17243b73adf4",
"assets/assets/bakabu/instruments/audio/flute.wav": "7539465d06685d19520d2c7c65f6448e",
"assets/assets/bakabu/instruments/audio/guitar.wav": "8d8a20b561d47c962c328377cc7a2b0c",
"assets/assets/bakabu/instruments/audio/contrabass.wav": "4f686649a14b784808fba53a4d232d5d",
"assets/assets/bakabu/instruments/audio/clarinete.wav": "a4ae04bb44610e71042aa1491fd1c779",
"assets/assets/bakabu/instruments/audio/trumpet.wav": "ded74fa190f769685860192bc6d0da74",
"assets/assets/bakabu/instruments/audio/tamburine.wav": "f816de7bdb33548f5aaf87cfb6d7bea9",
"assets/assets/bakabu/instruments/audio/chimes.wav": "6102b9f590adc4943e115728d8358828",
"assets/assets/bakabu/notes_big.svg": "8202d2a38ebe3cf391af7cfa7ecb38e2",
"assets/assets/bakabu/wave.svg": "f399f0fa6cad09e85075046cc2d8d3b5",
"assets/assets/in_mir_spielt_musik/background.svg": "b90cbdab885228c90ed0cc21d916b3d5",
"assets/assets/in_mir_spielt_musik/sound_yellow_inactive.svg": "cce61ee8181f0c4f1754fe54640ea3a1",
"assets/assets/in_mir_spielt_musik/sound_card_background.svg": "41ce7364d9a2ed3a79844adf0fe649dd",
"assets/assets/in_mir_spielt_musik/confetti.svg": "988b58b40a5e3a81e52695f2feb5179a",
"assets/assets/in_mir_spielt_musik/sound_pink_selected.svg": "542df954d6ec4ef7f56cb40952524141",
"assets/assets/in_mir_spielt_musik/sound_yellow.svg": "d232b5813a4a0336712ca9c101675e7f",
"assets/assets/in_mir_spielt_musik/audio/celebration_1.wav": "3e68e8fdf806f699f24520cd889bc9db",
"assets/assets/in_mir_spielt_musik/audio/celebration_3.wav": "2a66feb89fec5301f25887c2e8cc4ce1",
"assets/assets/in_mir_spielt_musik/audio/celebration_2.wav": "f1644b4a0aec4e2036cb2dbba3c5c447",
"assets/assets/in_mir_spielt_musik/audio/bee.wav": "116ac87e24b698d14c2a733597b2f9ea",
"assets/assets/in_mir_spielt_musik/audio/finger_snip.wav": "4ab4a7665005ca852895c8f72688958a",
"assets/assets/in_mir_spielt_musik/audio/wind.wav": "f1157b5ce57fdd027e144c0733580f7d",
"assets/assets/in_mir_spielt_musik/audio/dog.wav": "eb19376e789ca87488c8b7589fc5de0e",
"assets/assets/in_mir_spielt_musik/audio/duck.wav": "94c835fb89120e616d07bcdb25ac207f",
"assets/assets/in_mir_spielt_musik/audio/alarm_clock.wav": "7bd9f9eb5d4d5a29428730e5729c2022",
"assets/assets/in_mir_spielt_musik/audio/heart_beat.wav": "cccaee2157c527db7b67825613fbb015",
"assets/assets/in_mir_spielt_musik/audio/whistle.wav": "b6a0e7b0d9339953a14dbfde54828d57",
"assets/assets/in_mir_spielt_musik/audio/bus.wav": "201e4a2caede2aa351ab367015652817",
"assets/assets/in_mir_spielt_musik/audio/ship_horn.wav": "a6a9c034fa48acf03eb601e053de6240",
"assets/assets/in_mir_spielt_musik/audio/polic_siren.wav": "5b8efca22745387f2d0485bd8075bb84",
"assets/assets/in_mir_spielt_musik/audio/bicycle.wav": "35fe793894269df1c7aed5748c4b05b9",
"assets/assets/in_mir_spielt_musik/audio/rain.wav": "ee27f40f83226550e882d1defb748531",
"assets/assets/in_mir_spielt_musik/audio/bear.wav": "575a2e338b64a770aa90a176085e09cb",
"assets/assets/in_mir_spielt_musik/audio/tongue_click.wav": "5b2eb8137a3490d60c0a63d296383b39",
"assets/assets/in_mir_spielt_musik/audio/cat.wav": "689fac554e140186b5356179cb18db02",
"assets/assets/in_mir_spielt_musik/audio/hand_clap.wav": "f82297a40b1c24405300d2812b0f2e25",
"assets/assets/in_mir_spielt_musik/audio/car_horn.wav": "c702d7271e5e499049b8570f527a630e",
"assets/assets/in_mir_spielt_musik/audio/dove.wav": "a9c9e1d20fb5ef03deab39259ff9a1cc",
"assets/assets/in_mir_spielt_musik/audio/bird_twitter.wav": "6c021baed7442d01a93b36f7a9f321e6",
"assets/assets/in_mir_spielt_musik/audio/foot_stomp.wav": "4da998da16214c4d90a27bef7e5919e2",
"assets/assets/in_mir_spielt_musik/sound_pink_unactive.svg": "7839c886b11e5aa4a8afc9220d7e3e90",
"assets/assets/in_mir_spielt_musik/animations/pink_sound.json": "fc82c41724228a9e2c49a046ca33384b",
"assets/assets/roboto/Roboto-Regular.ttf": "8a36205bd9b83e03af0591a004bc97f4",
"assets/assets/animations/bakabu.json": "d9e3c8903fe1b582448758af4a79609e",
"assets/assets/animations/bkabu.png": "fc42ec23e9122041d1522dcd93008d9c",
"assets/assets/animations/celebrating.json": "1fc4ed371c5dce87e028842b96068e0f",
"canvaskit/skwasm.js": "5d4f9263ec93efeb022bb14a3881d240",
"canvaskit/skwasm.js.symbols": "c3c05bd50bdf59da8626bbe446ce65a3",
"canvaskit/canvaskit.js.symbols": "74a84c23f5ada42fe063514c587968c6",
"canvaskit/skwasm.wasm": "4051bfc27ba29bf420d17aa0c3a98bce",
"canvaskit/chromium/canvaskit.js.symbols": "ee7e331f7f5bbf5ec937737542112372",
"canvaskit/chromium/canvaskit.js": "901bb9e28fac643b7da75ecfd3339f3f",
"canvaskit/chromium/canvaskit.wasm": "399e2344480862e2dfa26f12fa5891d7",
"canvaskit/canvaskit.js": "738255d00768497e86aa4ca510cce1e1",
"canvaskit/canvaskit.wasm": "9251bb81ae8464c4df3b072f84aa969b",
"canvaskit/skwasm.worker.js": "bfb704a6c714a75da9ef320991e88b03"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"flutter_bootstrap.js",
"assets/AssetManifest.bin.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
