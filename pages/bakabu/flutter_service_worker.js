'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"flutter.js": "6fef97aeca90b426343ba6c5c9dc5d4a",
"main.dart.js": "70fbee8f2b80d39bbad22c0d75ebfccb",
"version.json": "cc6162f6df236e189ec168fc5a1df9d6",
"manifest.json": "4238a4b34004e06b74d158c60ec8a2cc",
"index.html": "20d325fdfb1d878d69e8814f2c538009",
"/": "20d325fdfb1d878d69e8814f2c538009",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"assets/shaders/ink_sparkle.frag": "f8b80e740d33eb157090be4e995febdf",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "89ed8f4e49bcdfc0b5bfc9b24591e347",
"assets/AssetManifest.json": "3a52e50fa5952d71b7b1e06054d25d22",
"assets/NOTICES": "fbed9d07e23fa9691f3c6ed3ab44e605",
"assets/assets/kamishibai_background.svg": "8b387613cd086184921f15fb2e4306d2",
"assets/assets/manual.svg": "e3a5f6c78cddf0b5dbcab7bd12dfeb40",
"assets/assets/auto.svg": "55ba3ae4d865f329b2a62f3158f96761",
"assets/assets/door.png": "061bb70cec5af8613cd31c89a5494850",
"assets/assets/back_button_animation.json": "d1ca0150f8ac9376e1de2c0d47186fb1",
"assets/assets/audio/creak.mp3": "13f1f92cb7257b79cbc1d3db2bd583d1",
"assets/assets/stories/story3/story_audio.mp3": "3eb9ae2008866c63cfc94f547917d399",
"assets/assets/stories/story3/frames/frame_4.jpg": "b10c7d109be019eac363027c3cfa6156",
"assets/assets/stories/story3/frames/frame_6.jpg": "cc3db89f1891bc63e83d0a9f5fa8f185",
"assets/assets/stories/story3/frames/frame_11.jpg": "72dc2ca7e1292ed9b86c8921f0c14a16",
"assets/assets/stories/story3/frames/frame_9.jpg": "73abfb4d09e91e95bec1a66069ff3068",
"assets/assets/stories/story3/frames/frame_2.jpg": "823719ef23ee87bed6e87b025c21381d",
"assets/assets/stories/story3/frames/frame_3.jpg": "061268be3e7c0caf691889da42462668",
"assets/assets/stories/story3/frames/frame_10.jpg": "7f07af41f11e1a1abc676e0369bf27c4",
"assets/assets/stories/story3/frames/frame_7.jpg": "0ca5a397c2324778066dc1e5faaaaf5f",
"assets/assets/stories/story3/frames/frame_1.jpg": "714706a1f57750f13666b4c16caf0740",
"assets/assets/stories/story3/frames/frame_5.jpg": "e68aff79ff52af8fb3a02bd4ff02c7e9",
"assets/assets/stories/story3/frames/frame_8.jpg": "d41cf63ee3cce6b82a1d92a6655406c5",
"assets/assets/stories/story1/story_audio.mp3": "9200945ee8726a0bf240d68041f95a87",
"assets/assets/stories/story1/cover.jpeg": "6e4306282338e6c94696014111cf4c36",
"assets/assets/stories/story1/frames/frame_4.jpg": "eb1e363f3c059aa67cd7814348e10584",
"assets/assets/stories/story1/frames/frame_1.jpeg": "e129e7be326489d5a95e1daa14486de6",
"assets/assets/stories/story1/frames/frame_6.jpg": "b8a172c6c93063e1d1ce6a79e0257241",
"assets/assets/stories/story1/frames/frame_11.jpg": "94df45453e2bb85844f733955b35e642",
"assets/assets/stories/story1/frames/frame_9.jpg": "a63953b2cf9c63a2975499cd60ecf10d",
"assets/assets/stories/story1/frames/frame_2.jpg": "09b3b4e1388cebdcb9ddcea1969ca2c8",
"assets/assets/stories/story1/frames/frame_3.jpg": "7f75b3eefac3025adfc5224c93a5269e",
"assets/assets/stories/story1/frames/frame_10.jpg": "1e80734e7dfee8769de284d6b8533cc5",
"assets/assets/stories/story1/frames/frame_12.jpg": "0b49d69b50a659a721b40455396da0cd",
"assets/assets/stories/story1/frames/frame_7.jpg": "48088cb59e10aad007d96c18d90d7ff1",
"assets/assets/stories/story1/frames/frame_5.jpg": "85b4e7799d9fe5d93d8f5ca81e5738d7",
"assets/assets/stories/story1/frames/frame_8.jpg": "e43de6026dd206005132abc8de89ace4",
"assets/assets/stories/story4/story_audio.mp3": "c15191813aba475ca9ca3ed4652574d7",
"assets/assets/stories/story4/sound_effects/fly.mp3": "5c2bab4bf063efbd12fc30d14b42bbd6",
"assets/assets/stories/story4/sound_effects/triangle.mp3": "18476c171eb115792792f63945b9f374",
"assets/assets/stories/story4/sound_effects/cry.mp3": "0f8d658607baec898a18b905aa55cb7a",
"assets/assets/stories/story4/sound_effects/triangle.jpg": "9ceb1ecd76f9e95d70302435bc312e72",
"assets/assets/stories/story4/sound_effects/cough.mp3": "ba69814a9d2c299a708ca1ae8277d58e",
"assets/assets/stories/story4/sound_effects/guitar.mp3": "fd3805d25debe0e06417e4d1d08ac604",
"assets/assets/stories/story4/sound_effects/cough.jpg": "bf2390e14bc8cdd1fd6f0adb84ce42f0",
"assets/assets/stories/story4/sound_effects/piano.mp3": "02b1d21d8e82571058035cbaf8db6b4b",
"assets/assets/stories/story4/sound_effects/baka_boogie.mp3": "eeba8c7dfae2c73fc0fd2e5191b17f88",
"assets/assets/stories/story4/sound_effects/boogie.jpg": "0467f28fe47a6d411f8d0506f404cefc",
"assets/assets/stories/story4/sound_effects/sneezing.mp3": "8a28ed2caa091f9157014b1373ea31ef",
"assets/assets/stories/story4/sound_effects/fly.jpg": "95a2864a66fb147805ac95ac007fc7e4",
"assets/assets/stories/story4/sound_effects/piano.jpg": "45f52e7248e449f222692d3dc10888c4",
"assets/assets/stories/story4/sound_effects/guitar.jpg": "2ec2324b3eb2a32b85f7473303681524",
"assets/assets/stories/story4/sound_effects/whistling.jpg": "745e5307a2f54d069ac48b91ff20f9a5",
"assets/assets/stories/story4/sound_effects/whistling.mp3": "86fe317df7292db627312e111f399790",
"assets/assets/stories/story4/sound_effects/sneezing.png": "09faa49ebb36448867875f0d6b641430",
"assets/assets/stories/story4/sound_effects/cry.jpg": "9eb911c88fd8114e8334e45461ecf01b",
"assets/assets/stories/story4/cover.jpg": "78e2d3a10a81af1d8bce7c3d567d888b",
"assets/assets/stories/story4/frames/frame_4.jpg": "9bcb4a44725c2251050bee2f5d712444",
"assets/assets/stories/story4/frames/frame_6.jpg": "dd76016bca298845603e8903685260b0",
"assets/assets/stories/story4/frames/frame_11.jpg": "e9f86f494ff02c203606aeecd5e47976",
"assets/assets/stories/story4/frames/frame_9.jpg": "ac50f18edaad82d8e5ad1cf9de07f9a1",
"assets/assets/stories/story4/frames/frame_2.jpg": "e02d76c825709cafa8d1b5e07c99cc59",
"assets/assets/stories/story4/frames/frame_3.jpg": "4d62fad76a29ca67c428076f7e23f189",
"assets/assets/stories/story4/frames/frame_10.jpg": "4dfbebe8749c1414e6244869fb2d6b61",
"assets/assets/stories/story4/frames/frame_12.jpg": "40795b5526a3c625404167ef5a56ace7",
"assets/assets/stories/story4/frames/frame_7.jpg": "2e52d2dc57ed2931344c9d67debc7ea6",
"assets/assets/stories/story4/frames/frame_1.jpg": "89752e570b51776db2f0a6dd3c7e64f2",
"assets/assets/stories/story4/frames/frame_5.jpg": "ad2c7198e4f44fee9d4479f04742dba8",
"assets/assets/stories/story4/frames/frame_8.jpg": "d723e5d56d6321bbc52f107ab2e6c7b5",
"assets/assets/stories/story2/story_audio.mp3": "e9ce5a2915dc88ce84b2ee6102af1c29",
"assets/assets/stories/story2/cover.jpg": "ba73a4dd295b6f29261918ae40b9ee3e",
"assets/assets/stories/story2/frames/frame_4.jpg": "149d22301a49a86a1e536f11d010103a",
"assets/assets/stories/story2/frames/frame_6.jpg": "227883eaebfc97040a34b0833633adde",
"assets/assets/stories/story2/frames/frame_11.jpg": "9f7adb0e05ed34ccd3c3c7042d586c70",
"assets/assets/stories/story2/frames/frame_9.jpg": "1d972c7d4640f2553f87880d44d2ea81",
"assets/assets/stories/story2/frames/frame_2.jpg": "819c4611e2c71a065d418b90ced29ea3",
"assets/assets/stories/story2/frames/frame_3.jpg": "777f54636dbc49a2d500b2d9df6a0c26",
"assets/assets/stories/story2/frames/frame_10.jpg": "70a9c4cb8fedea4e6ff01dd1b0475808",
"assets/assets/stories/story2/frames/frame_7.jpg": "4ef9646cec0f56dbbb1f6caf96be10a5",
"assets/assets/stories/story2/frames/frame_1.jpg": "009ff2d5d7bfd2bc8e42358a982e0b6c",
"assets/assets/stories/story2/frames/frame_5.jpg": "1426850e871813c1b4fdc225c2c7b47a",
"assets/assets/stories/story2/frames/frame_8.jpg": "154df0f0d4d77e04863a73ef258d5f05",
"assets/assets/background.png": "dbc657f881ed0123622cd1756f20fefa",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/AssetManifest.bin": "5bd87a56e0ff34d80ab9efd358c2c93d",
"assets/fonts/MaterialIcons-Regular.otf": "32fce58e2acb9c420eab0fe7b828b761",
"canvaskit/skwasm.worker.js": "51253d3321b11ddb8d73fa8aa87d3b15",
"canvaskit/canvaskit.js": "bbf39143dfd758d8d847453b120c8ebb",
"canvaskit/skwasm.js": "95f16c6690f955a45b2317496983dbe9",
"canvaskit/chromium/canvaskit.js": "96ae916cd2d1b7320fff853ee22aebb0",
"canvaskit/chromium/canvaskit.wasm": "be0e3b33510f5b7b0cc76cc4d3e50048",
"canvaskit/canvaskit.wasm": "42df12e09ecc0d5a4a34a69d7ee44314",
"canvaskit/skwasm.wasm": "1a074e8452fe5e0d02b112e22cdcf455",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"assets/AssetManifest.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
